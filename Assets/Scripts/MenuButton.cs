﻿using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour {

    #region Script's Summary
    //This script is performing the functions relating to the menu
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public Material _sphereMaterial;

    private Texture[] _allImages;
    private int _index;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {
        //--------- variable definitions -----------

        _index = 0;
        LoadImages();

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// load all images and save it in a variable
    /// </summary>
    void LoadImages()
    {
        _allImages = Resources.LoadAll<Texture>("Images");
    }

    public void NextImage()
    {
        //last image
        if (_index == _allImages.Length - 1)
            _index = 0;
        else
            _index++;

        _sphereMaterial.mainTexture = _allImages[_index];
    }

    public void PreviousImage()
    {
        //first image
        if (_index == 0)
            _index = _allImages.Length - 1;
        else
            _index--;

        _sphereMaterial.mainTexture = _allImages[_index];
    }

    //----------------------------------------------------------------------

    #endregion
}

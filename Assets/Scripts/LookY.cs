﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookY : MonoBehaviour {

	[SerializeField]  float _senstivity = 5f;
	public float MinimumY = -30f;
	public float MaximumY = 30f;
	private float _rotationY = 0f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		_rotationY += Input.GetAxis("Mouse Y") * _senstivity;
		_rotationY = Mathf.Clamp(_rotationY, MinimumY, MaximumY);
		transform.localEulerAngles=new Vector3(-_rotationY,transform.localEulerAngles.y,0);
	}
}

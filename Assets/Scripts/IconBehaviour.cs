﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBehaviour : MonoBehaviour
{

    #region Script's Summary
    //This script is managing the behaviour of categories item
    //which propertes to change when the reticle is over an item
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------
    
    Color currentColor;
    Image currentImage = null;

    float CountDownTimer = 1f;
    float currentTime = 0f;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    private void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
                UnGlowIcon();
        }
    }

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// glow the image
    /// </summary>
    /// <param name="img">image</param>
    public void GlowIcon(Image img)
    {
        if (currentImage == null)
        {
            currentImage = img;
            currentColor = img.color;
        }
        else if(currentImage.GetInstanceID() != img.GetInstanceID())
            UnGlowIcon();
        
        currentTime = CountDownTimer;
        IncreasedAlpha();
    }

    public void ChangeColor(Color c)
    {
        currentColor = c;
    }

    /// <summary>
    /// supported function
    /// </summary>
    void IncreasedAlpha()
    {
        if(currentImage)
            currentImage.color = new Color(currentColor.r, currentColor.g, currentColor.b, 1);
    }

    void UnGlowIcon()
    {
        currentImage.color = currentColor;
        currentImage = null;
    }

    //----------------------------------------------------------------------

    #endregion
}
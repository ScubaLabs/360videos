﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class camera360Images : MonoBehaviour {


    #region Script's Summary
    //This script is controlling  the interaction of reticle with the world
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public MenuButton _menuScript;

    private IconBehaviour _iconScript;
    private bool _isClick;
    private RaycastHit _hit;
    private GameObject _currentObject;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {
        //--------- variable definitions -----------

        _iconScript = GetComponent<IconBehaviour>();

        //-------------------------------------------
    }

    
	void Update () {

        Physics.Raycast(new Ray(transform.position, transform.forward), out _hit, 100f);

        if (_hit.collider == null || _hit.collider.CompareTag("Untagged"))
            return;

        _currentObject = _hit.collider.gameObject;

        DecideInput();
        HoverOrClick();
    }


    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// button input detection
    /// </summary>
    void DecideInput()
    {
        _isClick = false;

        //button A or X
        if (OVRInput.GetUp(OVRInput.Button.One))
            _isClick = true;

        else if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad))
        {
            // yes, let's require an actual click rather than just a touch.
            if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                _isClick = true;
        }

#if UNITY_EDITOR
        if (Input.GetButtonUp("Fire1"))
            _isClick = true;
#endif
    }

    /// <summary>
    /// actions based on hover or click
    /// </summary>
    void HoverOrClick()
    {
        if (_currentObject.CompareTag("Icon"))
        {
            _iconScript.GlowIcon(_currentObject.GetComponent<Image>());

            switch (_currentObject.name)
            {
                case "previous":
                    {
                        //ShowTip("Return to Main Categories");
                        if (_isClick)
                            _menuScript.PreviousImage();
                    }
                    break;
                case "next":
                    {
                        //ShowTip("Return the Selected Product");
                        if (_isClick)
                            _menuScript.NextImage();
                    }
                    break;
            }
        }
    }

    //----------------------------------------------------------------------

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoAds : MonoBehaviour {

    #region Script's Summary
    //This script is playing ads
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    private VideoClip[] allAds;
    private int currentVideoIndex;
    [HideInInspector]
    public string currentAdId;

    private VideoPlayer videoPlayer;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        allAds = Resources.LoadAll<VideoClip>("Video");
        videoPlayer = GetComponent<VideoPlayer>();
        currentVideoIndex = -1;
        ChangeClips();

        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// play a video clip
    /// </summary>
    void ChangeClips()
    {
        currentVideoIndex++;
        if (currentVideoIndex == allAds.Length)
            currentVideoIndex = 0;

        currentAdId = allAds[currentVideoIndex].name;
        videoPlayer.clip = allAds[currentVideoIndex];
        videoPlayer.Play();

        Invoke("ChangeClips", (float)(allAds[currentVideoIndex].length));
    }

    //----------------------------------------------------------------------

    #endregion
}